﻿using Canvas.Domain.Shared;
using System;
using Volo.Abp.Application;
using Volo.Abp.Modularity;

namespace Canvas.Application.Contracts
{
    [DependsOn(
        typeof(DomainSharedModule)
    )]
    public class ApplicationContractsModule : AbpModule
    {
    }
}
