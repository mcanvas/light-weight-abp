﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;

namespace Canvas.Application.Contracts
{
    public interface IAdService
    {
        Task<adDto> GetAsync(int id);
        Task<adDto> CreateAsync(adDto input);
        Task<adDto> UpdateAsync(adDto input);
        Task DeleteAsync(int id);
        Task<PagedResultDto<adDto>> GetListAsync(ADQueryDto input);

    }
}
