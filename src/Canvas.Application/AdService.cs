﻿using Canvas.Application.Contracts;
using Canvas.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace Canvas.Application
{
    public class AdService : ApplicationService, IAdService
    {
        private readonly IRepository<ad, int> _adRepository;

        public AdService(IRepository<ad, int> adRepository)
        {
            _adRepository = adRepository;
        }

        public async Task<adDto> CreateAsync(adDto input)
        {
            var DbModel = ObjectMapper.Map<adDto, ad>(input);
            DbModel = await _adRepository.InsertAsync(DbModel, true);
            return ObjectMapper.Map<ad, adDto>(DbModel);
            
        }

        public async Task DeleteAsync(int id)
        {
            await _adRepository.DeleteAsync(id);
        }

        public async Task<adDto> GetAsync(int id)
        {
            var DbModel = await _adRepository.GetAsync(id);
            return ObjectMapper.Map<ad, adDto>(DbModel);
        }

        public async Task<PagedResultDto<adDto>> GetListAsync(ADQueryDto input)
        {
            var pageResult = new PagedResultDto<adDto>();
            var Count = await _adRepository.CountAsync();
            var adList = await AsyncExecuter.ToListAsync(_adRepository.PageBy(input));
            pageResult.TotalCount = Count;
            pageResult.Items = ObjectMapper.Map<List<ad>, List<adDto>>(adList);
            return pageResult;

        }

        public async Task<adDto> UpdateAsync(adDto input)
        {
            var DbModel = await _adRepository.GetAsync(input.Id);
            DbModel = ObjectMapper.Map(input, DbModel);
            await _adRepository.UpdateAsync(DbModel);
            return input;
        }
    }
}
