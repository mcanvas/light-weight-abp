﻿using AutoMapper;
using Canvas.Application.Contracts;
using Canvas.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Canvas.Application
{
    public class ApplicationAutoMapperProfile : Profile
    {
        public ApplicationAutoMapperProfile()
        {
            /* 在这里配置AutoMapper映射配置. */
            CreateMap<ad, adDto>();
            CreateMap<adDto, ad>();
        }
    }
}
