﻿using Canvas.Domain.Shared;
using System;
using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace Canvas.Domain
{
    [DependsOn(
        typeof(AbpDddDomainModule)
    )]
    public class DomainModule : AbpModule
    {

    }
}
